from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.


# To access this view, a person must be logged in. @login_required


@login_required
def receipt_list(request):
    # Make sure that you filter the data for only the current user.
    # You can do this by using the .filter method instead of the .all method.
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        # inside the context should be the "related name"
        # of the model you are referring to
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)
# register in urls.py inside this app =
# path("", receipt_list, name="home")


# To access this view, a person must be logged in. @login_required


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)
# register in urls.py inside this app =
# path("create/", create_receipt, name="create_receipt")


# To access this view, a person must be logged in. @login_required


@login_required
def category_list(request):
    # Make sure that you filter the data for only the current user.
    # You can do this by using the .filter method instead of the .all method.
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        # inside the context should be the "related name"
        # of the model you are referring to
        "categories": categories,
    }
    return render(request, "categories/list.html", context)
# register in urls.py inside this app =
# path("categories/", category_list, name="category_list")


# you can refer to psuedo code above and relate it this view
@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


# to create this view you first need to create a form for it.
#  the form for this view is CategoryForm(forms.ModelForm):
@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            # When the ExpenseCategory is saved, "comes from the CategoryForm"
            # the owner must be set to the current user. see below
            category = form.save(False)
            category.owner = request.user
            # On a successful creation of an ExpenseCategory,
            # redirect the browser to the list of expense categories
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)
# register in urls.py inside this app =
# path("categories/create/", create_category, name="create_category"


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
